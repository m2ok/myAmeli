<?php
//Script vide la table specialite de la bd et la re remplit
//insertion des spécialités des ps: ce tableau de correspondance est dans la doc coding dojo car le webservice callService_DetailPS ramène entre autres la spécialité ps sous forme de code
include 'conf_bd.php';
$spe=array(
"01"=>"Médecin généraliste",
"02"=>"Anesthésiste réanimateur",
"03"=>"Cardiologue",
"04"=>"Chirurgien général",
"05"=>"Dermatologue vénérologue",
"06"=>"Radiologue",
"07"=>"Gynécologue obstétrique",
"08"=>"Gastro entérologue et hépatologue",
"09"=>"Médecin spécialiste en médecine interne",
"10"=>"Médecin neurochirurgien",
"11"=>"Médecin Oto-Rhino-Laryngologue (ORL) et chirurgien cervico-facial",
"12"=>"Pédiatre",
"13"=>"	Pneumologue",
"14"=>"	Rhumatologue",
"15"=>"	Ophtalmologue",
"16"=>"	Chirurgien urologue",
"17"=>"	NeuroPsychiatre",
"18"=>"	Stomatologue",
"19"=>"	Chirurgien-dentiste",
"20"=>"	Réanimateur",
"21"=>"	Sage femme",
"24"=>"	Infirmier",
"26"=>"	Kinésithérapeute",
"28"=>"	Orthophoniste",
"29"=>"	Orthoptiste",
"30"=>"	Laboratoire analyse médicale",
"31"=>"	Médecin spécialiste en médecine physique et de réadaptation",
"32"=>"	Neurologue",
"33"=>"	Psychiatre",
"34"=>"	Gériatre",
"35"=>"	Néphrologue",
"36"=>"	Chirurgien-dentiste spécialiste en orthopédie dento-faciale",
"37"=>"	Médecin anatomo-cyto-pathologiste",
"38"=>"	Médecin biologiste",
"39"=>"	Laboratoire",
"40"=>"	Laboratoire anathomo-pathologie",
"41"=>"	Chirurgien orthopédiste et traumatologue",
"42"=>"	Médecin endocrinologue – diabétologue",
"43"=>"	Chirurgien infantile",
"44"=>"	Chirurgien maxillo-facial",
"45"=>"	Chirurgien maxillo-facial et stomatologiste",
"46"=>"	Chirurgien plastique",
"47"=>"	Chirurgien thoracique et cardio-vasculaire",
"48"=>"	Chirurgien vasculaire",
"49"=>"	Chirurgien viscérale",
"50"=>"	Pharmacien",
"51"=>"	Pharmacien",
"53"=>"	Chirurgien-dentiste spécialiste en chirurgie orale",
"54"=>"	Chirurgien-dentiste spécialiste en médecine bucco-dentaire",
"55"=>"	Transporteur sanitaire (TAXI,,AMBULANCE et VSL)",
"56"=>"	Transporteur sanitaire (TAXI uniquement)",
"60"=>" 	Fournisseur de petits et grands appareillages",
"61"=>"     Fournisseur de petits et grands appareillages",
"62"=>"     Fournisseur de petits et grands appareillages",
"63"=>"     Fournisseur de petits et grands appareillages ",
"64 "=>"     Fournisseur de petits et grands appareillages",
"65 "=>"     Fournisseur de petits et grands appareillages",
"66 "=>"     Fournisseur de petits et grands appareillages",
"67 "=>"     Fournisseur de petits et grands appareillages",
"68"=>"     Fournisseur de petits et grands appareillages",
"69"=>"	Chirurgien orale",
"70"=>"	Médecin gynécologue médical",
"71"=>"	Médecin hématologue",
"73"=>"	Cancérologue",
"74"=>"	Cancérologue radiothérapeute",
"75"=>"	Médecin psychiatre de l’enfant et de l’adolescent",
"76"=>"	Radiothérapeute",
"77"=>"	Médecin obstétricien",
"78"=>"	Médecin en génétique médicale ",
"79"=>"	Médecin gynécologue obstétrique et gynécologue médicale",
"80"=>"	Médecin spécialiste en santé publique et médecine sociale"
);

try
{
    $bdd = new PDO('mysql:host=localhost;dbname='.$bd_name.';charset=utf8', $db_login, $db_password);
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}
$stmt = $bdd->prepare("delete from specialite");
$stmt->execute();

foreach ($spe as $key => $value) {
    $key=trim($key);
    $value=trim($value);
    echo $key." ".$value."<br/>";
    $stmt = $bdd->prepare("INSERT INTO specialite (code_spe,libelle_spe) VALUES (?,?)");
    $stmt->bindParam(1, $key);
    $stmt->bindParam(2, $value);

    $stmt->execute();
}
