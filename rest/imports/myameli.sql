-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.6.17 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la base pour myameli
DROP DATABASE IF EXISTS `myameli`;
CREATE DATABASE IF NOT EXISTS `myameli` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `myameli`;


-- Export de la structure de table myameli. assure
DROP TABLE IF EXISTS `assure`;
CREATE TABLE IF NOT EXISTS `assure` (
  `nir` varchar(15) NOT NULL,
  `nom` varchar(100) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `dateNaissance` date NOT NULL,
  `rang` smallint(6) NOT NULL,
  `qualite` char(1) NOT NULL,
  `sexe` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table myameli.assure : ~15 rows (environ)
DELETE FROM `assure`;
/*!40000 ALTER TABLE `assure` DISABLE KEYS */;
INSERT INTO `assure` (`nir`, `nom`, `prenom`, `avatar`, `dateNaissance`, `rang`, `qualite`, `sexe`) VALUES
	('1700801999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1970-08-29', 1, 'A', 'H'),
	('1550101999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1955-01-29', 1, 'A', 'H'),
	('1520139999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1952-01-29', 1, 'A', 'H'),
	('1520139999999', 'NOM02 PAT', 'PRENOM02', 'homme-1.png', '1952-01-30', 1, 'C', 'H'),
	('1520139999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1952-01-29', 1, 'A', 'H'),
	('1520139999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1952-01-30', 1, 'C', 'H'),
	('1640767999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1964-07-29', 1, 'A', 'H'),
	('1210539999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1921-05-29', 1, 'A', 'H'),
	('1483099999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1948-12-29', 1, 'A', 'H'),
	('2752099999999', 'NOM PAT', 'PRENOM', 'femme-1.png', '1975-12-29', 1, 'A', 'F'),
	('2790169999999', 'NOM PAT', 'PRENOM', 'femme-1.png', '1979-01-29', 1, 'A', 'F'),
	('279012B999999', 'NOM PAT', 'PRENOM', 'femme-1.png', '1979-01-29', 1, 'A', 'F'),
	('176092A999999', 'LECA', 'PIERRE-JEAN', 'homme-1.png', '1976-09-29', 1, 'A', 'H'),
	('1402093999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1940-12-29', 1, 'A', 'H'),
	('1305599999999', 'NOM PAT', 'PRENOM', 'homme-1.png', '1930-12-29', 1, 'A', 'H');
/*!40000 ALTER TABLE `assure` ENABLE KEYS */;


-- Export de la structure de table myameli. benef_assure
DROP TABLE IF EXISTS `benef_assure`;
CREATE TABLE IF NOT EXISTS `benef_assure` (
  `nir_benef` varchar(15) NOT NULL,
  `nir_assure` varchar(15) NOT NULL,
  `nom_benef` varchar(100) NOT NULL,
  `prenom_benef` varchar(100) NOT NULL,
  `dateNaissance_benef` date NOT NULL,
  `avatar_benef` varchar(200) NOT NULL,
  `rang_benef` smallint(6) NOT NULL,
  `qualite_benef` char(1) NOT NULL,
  `sexe_benef` char(1) NOT NULL,
  PRIMARY KEY (`nir_benef`),
  KEY `nir_assure` (`nir_assure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table myameli.benef_assure : ~5 rows (environ)
DELETE FROM `benef_assure`;
/*!40000 ALTER TABLE `benef_assure` DISABLE KEYS */;
INSERT INTO `benef_assure` (`nir_benef`, `nir_assure`, `nom_benef`, `prenom_benef`, `dateNaissance_benef`, `avatar_benef`, `rang_benef`, `qualite_benef`, `sexe_benef`) VALUES
	('1920399999999', '2752099999999', 'Durand', 'Marcus', '1995-03-25', 'garcon-1.png', 1, 'E', 'H'),
	('1940999999999', '2752099999999', 'Durand', 'Camille', '1997-09-05', 'femme-3.png', 1, 'E', 'F'),
	('1950539999999', '2752099999999', 'Durand', 'Nathan', '1999-05-20', 'garcon-2.png', 1, 'E', 'H'),
	('1960639999999', '2752099999999', 'Dupont', 'Martin', '2001-06-06', 'garcon-3.png', 1, 'E', 'H'),
	('2911099999999', '2752099999999', 'Durand', 'Fanny', '1975-10-29', 'femme-1.png', 1, 'A', 'F');
/*!40000 ALTER TABLE `benef_assure` ENABLE KEYS */;


-- Export de la structure de table myameli. favoris_ps_assures
DROP TABLE IF EXISTS `favoris_ps_assures`;
CREATE TABLE IF NOT EXISTS `favoris_ps_assures` (
  `nir` varchar(15) NOT NULL,
  `est_medecin_traitant` smallint(6) NOT NULL,
  `ps_num` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table myameli.favoris_ps_assures : ~9 rows (environ)
DELETE FROM `favoris_ps_assures`;
/*!40000 ALTER TABLE `favoris_ps_assures` DISABLE KEYS */;
INSERT INTO `favoris_ps_assures` (`nir`, `est_medecin_traitant`, `ps_num`) VALUES
	('1550101999999', 1, '011013554'),
	('1520139999999', 1, '391999505'),
	('1210539999999', 1, '011011509'),
	('1483099999999', 1, '011010311'),
	('2752099999999', 1, '011019056'),
	('2790169999999', 1, '591092903'),
	('176092A999999', 1, '011016631'),
	('1402093999999', 1, '011009453'),
	('1305599999999', 1, '011013224');
/*!40000 ALTER TABLE `favoris_ps_assures` ENABLE KEYS */;


-- Export de la structure de table myameli. ps
DROP TABLE IF EXISTS `ps`;
CREATE TABLE IF NOT EXISTS `ps` (
  `ps_num` varchar(50) NOT NULL,
  `ps_nom` varchar(100) NOT NULL,
  `ps_prenom` varchar(100) NOT NULL,
  `ps_telephone` varchar(20) NOT NULL,
  `ps_adresse` varchar(500) NOT NULL,
  `ps_civ` varchar(3) NOT NULL,
  `type_conv` varchar(300) NOT NULL,
  `ps_code_spe` varchar(2) NOT NULL,
  `cv` varchar(5) NOT NULL,
  PRIMARY KEY (`ps_num`),
  UNIQUE KEY `ps_num` (`ps_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table myameli.ps : ~18 rows (environ)
DELETE FROM `ps`;
/*!40000 ALTER TABLE `ps` DISABLE KEYS */;
INSERT INTO `ps` (`ps_num`, `ps_nom`, `ps_prenom`, `ps_telephone`, `ps_adresse`, `ps_civ`, `type_conv`, `ps_code_spe`, `cv`) VALUES
	('011009453', 'GARRON', 'Jean Paul', '0474813250', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('011010311', 'MAURY', 'Marie Hélène', '0450567100', '21 rue Georges Auric, 75019 Paris', 'MME', 'Conventionné de secteur 1', '01', 'true'),
	('011011269', 'TREMOLET', 'Jean Paul', '0474224329', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '12', 'true'),
	('011011509', 'BESSON', 'Jean Marc', '0474300990', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('011013224', 'ROMENTEAU', 'Christian', '0474320181', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('011013505', 'DURAND', 'Jean', '0474236838', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 2 sans CAS', '18', 'true'),
	('011013554', 'BORNAREL', 'Michel', '0385361212', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('011015815', 'MOREL VULLIEZ', 'Jean Bernard', '0474730752', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '18', 'true'),
	('011016631', 'PACCALLET', 'Guy', '0145637283', '21 rue Georges Auric, 75019 Paris', 'M', 'Honoraires sans dépassement', '01', 'true'),
	('011017878', 'ARNOULD', 'Sophie', '0474453719', '21 rue Georges Auric, 75019 Paris', 'MME', 'Conventionné de secteur 2 sans CAS', '12', 'true'),
	('011019056', 'JACQUIOT', 'Denis', '0479811713', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('011020229', 'DUSSEUX', 'Pascal', '0474223630', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '15', 'true'),
	('014006829', 'RUTHY', 'Jean François', '0450560914', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné', '19', 'true'),
	('201400439', 'BRUNET GAMET', 'Vanina', '0495333490', '21 rue Georges Auric, 75019 Paris', 'MME', 'Conventionné de secteur 1', '01', 'true'),
	('201413457', 'VINCENT', 'FLORENCE', '0495333490', '21 rue Georges Auric, 75019 Paris', 'MME', 'Conventionné de secteur 1', '01', 'true'),
	('391999505', 'PERROD', 'Matthieu', '0384476248', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('531004703', 'DOUETTE', 'Roland', '0243052013', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true'),
	('591092903', 'MASCIONI', 'Jean Claude', '0320250170', '21 rue Georges Auric, 75019 Paris', 'M', 'Conventionné de secteur 1', '01', 'true');
/*!40000 ALTER TABLE `ps` ENABLE KEYS */;


-- Export de la structure de table myameli. rdv
DROP TABLE IF EXISTS `rdv`;
CREATE TABLE IF NOT EXISTS `rdv` (
  `id_rdv` int(11) NOT NULL AUTO_INCREMENT,
  `id_assure` int(11) NOT NULL,
  `id_benef_assure` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `note` varchar(500) NOT NULL COMMENT 'commentaire libre',
  `ps_num` varchar(50) NOT NULL,
  `ps_nom` varchar(100) NOT NULL,
  `ps_prenom` varchar(100) NOT NULL,
  `ps_adresse` varchar(200) NOT NULL,
  `ps_telephone` varchar(20) NOT NULL,
  PRIMARY KEY (`id_rdv`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Export de données de la table myameli.rdv : ~0 rows (environ)
DELETE FROM `rdv`;
/*!40000 ALTER TABLE `rdv` DISABLE KEYS */;
/*!40000 ALTER TABLE `rdv` ENABLE KEYS */;


-- Export de la structure de table myameli. specialite
DROP TABLE IF EXISTS `specialite`;
CREATE TABLE IF NOT EXISTS `specialite` (
  `code_spe` int(10) NOT NULL,
  `libelle_spe` varchar(200) NOT NULL,
  `group` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`code_spe`),
  UNIQUE KEY `code_spe` (`code_spe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- Export de données de la table myameli.specialite : ~71 rows (environ)
DELETE FROM `specialite`;
/*!40000 ALTER TABLE `specialite` DISABLE KEYS */;
INSERT INTO `specialite` (`code_spe`, `libelle_spe`, `group`) VALUES
	(1, 'Médecin généraliste', ''),
	(2, 'Anesthésiste réanimateur', ''),
	(3, 'Cardiologue', ''),
	(4, 'Chirurgien général', ''),
	(5, 'Dermatologue vénérologue', ''),
	(6, 'Radiologue', ''),
	(7, 'Gynécologue obstétrique', ''),
	(8, 'Gastro entérologue et hépatologue', ''),
	(9, 'Médecin spécialiste en médecine interne', ''),
	(10, 'Médecin neurochirurgien', ''),
	(11, 'Médecin Oto-Rhino-Laryngologue (ORL) et chirurgien cervico-facial', ''),
	(12, 'Pédiatre', ''),
	(13, 'Pneumologue', ''),
	(14, 'Rhumatologue', ''),
	(15, 'Ophtalmologue', ''),
	(16, 'Chirurgien urologue', ''),
	(17, 'NeuroPsychiatre', ''),
	(18, 'Stomatologue', ''),
	(19, 'Chirurgien-dentiste', ''),
	(20, 'Réanimateur', ''),
	(21, 'Sage femme', ''),
	(24, 'Infirmier', ''),
	(26, 'Kinésithérapeute', ''),
	(28, 'Orthophoniste', ''),
	(29, 'Orthoptiste', ''),
	(30, 'Laboratoire analyse médicale', ''),
	(31, 'Médecin spécialiste en médecine physique et de réadaptation', ''),
	(32, 'Neurologue', ''),
	(33, 'Psychiatre', ''),
	(34, 'Gériatre', ''),
	(35, 'Néphrologue', ''),
	(36, 'Chirurgien-dentiste spécialiste en orthopédie dento-faciale', ''),
	(37, 'Médecin anatomo-cyto-pathologiste', ''),
	(38, 'Médecin biologiste', ''),
	(39, 'Laboratoire', ''),
	(40, 'Laboratoire anathomo-pathologie', ''),
	(41, 'Chirurgien orthopédiste et traumatologue', ''),
	(42, 'Médecin endocrinologue – diabétologue', ''),
	(43, 'Chirurgien infantile', ''),
	(44, 'Chirurgien maxillo-facial', ''),
	(45, 'Chirurgien maxillo-facial et stomatologiste', ''),
	(46, 'Chirurgien plastique', ''),
	(47, 'Chirurgien thoracique et cardio-vasculaire', ''),
	(48, 'Chirurgien vasculaire', ''),
	(49, 'Chirurgien viscérale', ''),
	(50, 'Pharmacien', ''),
	(51, 'Pharmacien', ''),
	(53, 'Chirurgien-dentiste spécialiste en chirurgie orale', ''),
	(54, 'Chirurgien-dentiste spécialiste en médecine bucco-dentaire', ''),
	(55, 'Transporteur sanitaire (TAXI,,AMBULANCE et VSL)', ''),
	(56, 'Transporteur sanitaire (TAXI uniquement)', ''),
	(60, 'Fournisseur de petits et grands appareillages', ''),
	(61, 'Fournisseur de petits et grands appareillages', ''),
	(62, 'Fournisseur de petits et grands appareillages', ''),
	(63, 'Fournisseur de petits et grands appareillages', ''),
	(64, 'Fournisseur de petits et grands appareillages', ''),
	(65, 'Fournisseur de petits et grands appareillages', ''),
	(66, 'Fournisseur de petits et grands appareillages', ''),
	(67, 'Fournisseur de petits et grands appareillages', ''),
	(68, 'Fournisseur de petits et grands appareillages', ''),
	(69, 'Chirurgien orale', ''),
	(70, 'Médecin gynécologue médical', ''),
	(71, 'Médecin hématologue', ''),
	(73, 'Cancérologue', ''),
	(74, 'Cancérologue radiothérapeute', ''),
	(75, 'Médecin psychiatre de l’enfant et de l’adolescent', ''),
	(76, 'Radiothérapeute', ''),
	(77, 'Médecin obstétricien', ''),
	(78, 'Médecin en génétique médicale', ''),
	(79, 'Médecin gynécologue obstétrique et gynécologue médicale', ''),
	(80, 'Médecin spécialiste en santé publique et médecine sociale', '');
/*!40000 ALTER TABLE `specialite` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
