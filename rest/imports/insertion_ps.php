<?php

//Vide la table sql des PS et la re remplit
//sources données insérées: va chercher les informations du mock OW_J pour chaque numero ps du tableau php ci dessous $tab_ps
// les numéro PS sont donnés dans la doc mock ps
ini_set('soap.wsdl_cache_enabled', 0);// Pour d�sactiver la mise en cache du wsdl
require_once 'SoapClientTimeout.php';

require_once 'conf_bd.php';

$tab_ps=array("014006829"
        ,"014010771"
        ,"531004703"
        ,"011013505"
        ,"011013554"
        ,"011011509"
        ,"011019056"
        ,"011009453"
        ,"011013224"
        ,"011015815"
        ,"391999505"
        ,"011010311"
        ,"591092903"
        ,"011011269"
        ,"011017878"
        ,"201413457"
        ,"201400439"
        ,"201413457"
        ,"201400439"
        ,"011802725"
        ,"011020229"
            );


try
{
    $bdd = new PDO('mysql:host=localhost;dbname='.$bd_name.';charset=utf8', $db_login, $db_password);
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}

$stmt = $bdd->prepare("delete from ps");
$ok=$stmt->execute();
if($ok) echo "PS supprimés";else { die ("Erreur suppression ps"); }
//Sur le portable a priori on ne peut pas utiliser la classe simple SoapClient de PHP brute, il faut utiliser SoapClientTimeout celle la qui �tend la soap normale
// Si on utilise la classe SOAP normale sur le portable message erreur TimeOut..
// cette nouvelle classe poss�de 2 m�thodes une m�thode __setTimeout(int) qu'il faut appeler et une qui r��crit __doRequest de SOAP qui est appel� automatiquement lors de la fonction __call

$chemin_du_wsdl='http://digital2.cn.cnamts.fr/MockServer3/OW_J?wsdl'; //callService_DetailEtablissement ou callService_DetailPS
$chemin_du_server="http://digital2.cn.cnamts.fr/MockServer3/OW_J";
$espace_nom="http://digital2.cn.cnamts.fr/MockServer3/OW_J";
//201413457

        
try	{
    $client = new SoapClientTimeout($chemin_du_wsdl, 
                                    array('trace'    => 1)
                            );
    $client->__setTimeout(200); //sinon erreur timeout
    $client->__setLocation ($chemin_du_server);//sinon erreur looks like we got no XML document 


    for($i=0;$i<count($tab_ps);$i++)
    {
        
        
        $params=array("NumAm" => $tab_ps[$i]);
        $return = $client->__soapCall("callService_DetailPS",array($params));

        $ps_num=trim($return->NumAm);
        $ps_nom=trim($return->ExeNom);
        $ps_prenom=trim($return->PfsPrn);
        $ps_telephone=trim($return->TelNum);
        $ps_adresse=trim($return->VoiLib.", ".$return->BdiCod." ".$return->RsdLib);
        $ps_code_spe=trim($return->ExeSpe);
        $ps_civ=trim($return->CivCod);
        $cv=trim($return->FseCod); //Une balise <FseCod>X</FseCod> signifie que le PS utilise la carte Vitale.La balise <FseCod> est absente dans le cas contraire.
        if($cv=="X")$cv="true";else $cv="false";
        
        
        //affiner via la doc et regle de gestion Type de conventionnement du PS via la doc Coding_Dojo_BO_ameli-direct_STD_WS et les combinaisons des 4 variables ci dessous
        $ps_type_offreur_de_soins=trim($return->TypOds);
        $MepCod=trim($return->MepCod);
        $CnvCod=trim($return->CnvCod);
        $CAS=trim($return->CAS);
        if($ps_type_offreur_de_soins=="01" || $ps_type_offreur_de_soins=="02")
        {
        
            if($CnvCod=="0")
                $type="Non conventionné";
            else if($CnvCod=="1")
                $type="Conventionné de secteur 1";
            else if($CnvCod=="2" && $CAS=="1")
                $type="Conventionné de secteur 1DP avec CAS";
            else if($CnvCod=="2" && $CAS=="0")
                $type="Conventionné de secteur 1DP sans CAS";
            else if($CnvCod=="3" && $CAS=="1")
                $type="Conventionné de secteur 2 avec CAS";
            else 
                $type="Conventionné de secteur 2 sans CAS";
        }else if($ps_type_offreur_de_soins=="03"){
            if($CnvCod=="0")
                $type="Non conventionné";
            else if($CnvCod=="1")
                $type="Conventionné";
            else 
                $type="Conventionné avec droit permanent à dépassement";
        }else if($ps_type_offreur_de_soins=="04" || $ps_type_offreur_de_soins=="05" || $ps_type_offreur_de_soins=="08"){
            if($CnvCod=="0")
                $type="Non conventionné";
            else 
                $type="Conventionné";    
        }else {
             $type="Non conventionné";
        }

        echo "<br/><br/><br/>Insertion du ps :".$ps_num."<br/>";
        print_r($return);

        $stmt = $bdd->prepare("INSERT INTO ps (ps_num,ps_nom,ps_prenom,ps_telephone,ps_adresse,ps_code_spe,ps_civ,type_conv,cv) VALUES (?,?,?,?,?,?,?,?,?) ");
        $stmt->bindParam(1, $ps_num);
        $stmt->bindParam(2, $ps_nom);
        $stmt->bindParam(3, $ps_prenom);
        $stmt->bindParam(4, $ps_telephone);
        $stmt->bindParam(5, $ps_adresse);
        $stmt->bindParam(6, $ps_code_spe);
        $stmt->bindParam(7, $ps_civ);
        $stmt->bindParam(8, $type);
        $stmt->bindParam(9, $cv);
        $ok=$stmt->execute();
        if(!$ok)
            echo "<span style='color:red'>Erreur pour ".$ps_num." <span>";

        
    }

}
catch (Exception $e)	{
    //echo '<br />Requete SOAP : '.htmlspecialchars($client->__getLastRequest()).'<br />';
    //echo '<br />Reponse SOAP : '.$client->__getLastResponse().'<br />';
    echo "<span style='color:red'> :".$e."<span>";
}
?>