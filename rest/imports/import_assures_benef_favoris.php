<?php

include 'conf_bd.php';

try
{
    $bdd = new PDO('mysql:host=localhost;dbname='.$bd_name.';charset=utf8', $db_login, $db_password);
}
catch(Exception $e)
{
    die('Erreur : '.$e->getMessage());
}

//vide les tables
$bdd->query('delete from assure');
$bdd->query('delete from benef_assure');
$bdd->query('delete from favoris_ps_assures');
//$bdd->query('delete from ps');
//$bdd->query('delete from paiement');



/******************************************************************/
/*
 * ASSURES ET BENEF ET MDC TRAITANT (favoris)
 */
/******************************************************************/
$fichier = 'xml/assures.xml';
$document_xml = new DomDocument();
$document_xml->load($fichier);

$listeAssures= $document_xml->getElementsByTagName('Assures');
  foreach($listeAssures as $assure){
    $nir = trim($assure->getElementsByTagName("nir")->item(0)->nodeValue);
    $rang= trim($assure->getElementsByTagName("rang")->item(0)->nodeValue);
    $nom= trim($assure->getElementsByTagName("nomPatronymique")->item(0)->nodeValue);
    $prenom= trim($assure->getElementsByTagName("prenom")->item(0)->nodeValue);
    $dateNaissance= trim($assure->getElementsByTagName("dateNaissance")->item(0)->nodeValue);
    $nirUnique= trim($assure->getElementsByTagName("nirUnique")->item(0)->nodeValue);
    $qualite = trim($assure->getElementsByTagName("qualite")->item(0)->nodeValue);
    
    
    if(substr($nir, 0,1)=="1"){
        $avatar="homme-1.png";
        $sexe="H";
    }   
    else{ 
        $avatar="femme-1.png";
        $sexe="F";
    }
    $alerte="";
    if($nirUnique=="false")
        $alerte="<span style='color:red'>Attention nir non unique !!</span>";
    
    echo "<br/><br/><span style='color:blue'>insertion de l'assuré :".$nir." ".$rang." ".$nom." ".$prenom." ".$dateNaissance." ".$avatar."</span>".$alerte."<br/>";
    
    //faire insertin assure
    $stmt = $bdd->prepare("INSERT INTO assure (nir,nom,prenom,dateNaissance,avatar,rang,qualite,sexe) VALUES (?,?,?,STR_TO_DATE(?, '%Y%m%d'),?,?,?,?)");
    $stmt->bindParam(1, $nir);
    $stmt->bindParam(2, $nom);
    $stmt->bindParam(3, $prenom);
    $stmt->bindParam(4, $dateNaissance);
    $stmt->bindParam(5, $avatar);
    $stmt->bindParam(6, $rang);
    $stmt->bindParam(7, $qualite);
    $stmt->bindParam(8, $sexe);
    $stmt->execute();
 
    
    echo "<span style='color:pink'>";
    $liste_medecin_t = $assure->getElementsByTagName("InfosMedecinTraitant");
    foreach($liste_medecin_t as $medecin_t){
         $statut=trim($medecin_t->getElementsByTagName("statut")->item(0)->nodeValue);
         if($statut=="TROUVE")
         {
                $ps_num=trim($medecin_t->getElementsByTagName("numero")->item(0)->nodeValue);
                $ps_code_spe=trim($medecin_t->getElementsByTagName("code")->item(0)->nodeValue);
                $voieNumero=trim($medecin_t->getElementsByTagName("voieNumero")->item(0)->nodeValue);
                $voieType=trim($medecin_t->getElementsByTagName("voieType")->item(0)->nodeValue);
                $voie=trim($medecin_t->getElementsByTagName("voie")->item(0)->nodeValue);
                $codePostal=trim($medecin_t->getElementsByTagName("codePostal")->item(0)->nodeValue);
                $commune=trim($medecin_t->getElementsByTagName("commune")->item(0)->nodeValue); 
                $ps_civ=trim($medecin_t->getElementsByTagName("civilite")->item(0)->nodeValue);
                $ps_nom=trim($medecin_t->getElementsByTagName("nomUsage")->item(0)->nodeValue);
                $ps_prenom=trim($medecin_t->getElementsByTagName("prenom")->item(0)->nodeValue);
                $ps_adresse=$voieNumero." ".$voieType." ".$voie.", ".$codePostal." ".$commune;
                $ps_telephone="0145637283";
                $type="Honoraires sans dépassement";
                $cv="true";
                $query = $bdd->prepare("Select * from ps where ps_num ='".$ps_num."'");
                $query->execute();
                $row = $query->fetch();
                if($row==false)
                {//insertion du ps dans la table ps
                    $stmt = $bdd->prepare("INSERT INTO ps (ps_num,ps_nom,ps_prenom,ps_telephone,ps_adresse,ps_code_spe,ps_civ,type_conv,cv) VALUES (?,?,?,?,?,?,?,?,?) ");
                    $stmt->bindParam(1, $ps_num);
                    $stmt->bindParam(2, $ps_nom);
                    $stmt->bindParam(3, $ps_prenom);
                    $stmt->bindParam(4, $ps_telephone);
                    $stmt->bindParam(5, $ps_adresse);
                    $stmt->bindParam(6, $ps_code_spe);
                    $stmt->bindParam(7, $ps_civ);
                    $stmt->bindParam(8, $type);
                    $stmt->bindParam(9, $cv);
                    $ok=$stmt->execute();
                    if(($ok))
                        echo "Medecin traitant ".$ps_num." (vient detre créé dans la bd)";
                } else {
                    echo "Medecin traitant ".$ps_num;
                } 
                $stmt = $bdd->prepare("INSERT INTO favoris_ps_assures (ps_num,nir,est_medecin_traitant) VALUES (?,?,?) ");
                $est_medecin_traitant=1;
                $stmt->bindParam(1, $ps_num);
                $stmt->bindParam(2, $nir);
                $stmt->bindParam(3, $est_medecin_traitant);
                $ok=$stmt->execute();
         }else 
             echo "PAS DE medecin traitant";
         
    }echo "</span><br/>";
    
    echo "<span style='color:green'>Beneficiaires de l'assuré:<br/>";
    $listeBeneficiaires = $assure->getElementsByTagName("Beneficiaires");
    foreach($listeBeneficiaires as $benef){
        $nir_benef=trim($benef->getElementsByTagName("nir")->item(0)->nodeValue);
        $nom_benef=trim($benef->getElementsByTagName("nomPatronymique")->item(0)->nodeValue);
        $prenom_benef=trim($benef->getElementsByTagName("prenom")->item(0)->nodeValue);
        $dateNaissance_benef=trim($benef->getElementsByTagName("dateNaissance")->item(0)->nodeValue);
        $rang_benef=trim( $benef->getElementsByTagName("rang")->item(0)->nodeValue);
        $qualite_benef=trim( $benef->getElementsByTagName("qualite")->item(0)->nodeValue);
        
        $annee_naiss=  intval(substr($dateNaissance_benef, 0,4));
        if(substr($nir_benef, 0,1)=="1"){
            $sexe_benef="H";
            if($annee_naiss<2000)
                $avatar_benef="homme-1.png";
            else
                $avatar_benef="garcon-1.png";
        }
        else {
            $sexe_benef="F";
            if($annee_naiss<2000)
                $avatar_benef="femme-2.png";
            else
                $avatar_benef="femme-1.png";
        }
        echo "insertion du benef :".$nir_benef." ".$rang_benef." ".$nom_benef." ".$prenom_benef." ".$dateNaissance_benef." ".$avatar_benef."<br/>";
        
        //faire insertin assure
        $stmt = $bdd->prepare("INSERT INTO benef_assure (nir_assure,nir_benef,nom_benef,prenom_benef,dateNaissance_benef,avatar_benef,rang_benef,qualite_benef,sexe_benef) VALUES (?,?,?,?,STR_TO_DATE(?, '%Y%m%d'),?,?,?,?)");
        $stmt->bindParam(1, $nir);
        $stmt->bindParam(2, $nir_benef);
        $stmt->bindParam(3, $nom_benef);
        $stmt->bindParam(4, $prenom_benef);
        $stmt->bindParam(5, $dateNaissance_benef);
        $stmt->bindParam(6, $avatar_benef);
        $stmt->bindParam(7, $rang_benef);
        $stmt->bindParam(8, $qualite_benef);
        $stmt->bindParam(9, $sexe_benef);
        $ok=$stmt->execute();
        
        
    }
    echo "</span>";
    
  }//for $listeAssures
  
  
  
  
  
