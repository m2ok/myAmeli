<?php

/*
  Paramètres accès mock
 */

//ini_set('soap.wsdl_cache_enabled', 0);// Pour désactiver la mise en cache du wsdl

require_once './class/SoapClientTimeout.php';

//$chemin_du_wsdl = 'http://digital2.cn.cnamts.fr/MockServer3/OW_J?wsdl';
//$chemin_du_server = "http://digital2.cn.cnamts.fr/MockServer3/OW_J";

/*
 * URL des Mocks
 */
$chemin_du_wsdl = 'http://120.0.0.20/MockServer3/OW_J?wsdl';
$chemin_du_server = "http://120.0.0.20/MockServer3/OW_J";

try{

    $client = new SoapClientTimeout($chemin_du_wsdl , array('trace' => 1));

    $client -> __setTimeout(200); //sinon erreur timeout

    $client -> __setLocation($chemin_du_server); //sinon erreur looks like we got no XML document

}catch(Exception $e){

    //echo '<br />Requete SOAP : '.htmlspecialchars($client->__getLastRequest()).'<br />';
    //echo '<br />Reponse SOAP : '.$client->__getLastResponse().'<br />';
    //echo "<span style='color:red'> :".$e."<span>";
    die("Erreur de connexion au Mock PS");
}