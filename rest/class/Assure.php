<?php

/*
  Ce script permet :
  1 - Lister les benef
 */

class Assure{
    /*
      @request : paramètres de l'URL
     */

    public $request;

    /*
      @action : action demandée
     */
    private $action;

    /*
      @user : NIR
     */
    private $user;

    /*
      @id : ID
     */
    private $id;

    /*
      @data : données au format JSON
     */
    private $data;

    function __construct($request){

        $this -> request = $request;

        $this -> action = $this -> request['action'];

        switch($this -> action){

            case 'getBenefs':

                $this -> user = $this -> request['user'];

                $this -> listBenef();

                break;
        }
    }

    /*
      Liste tous les benef d'un utilisateur
     */

    function listBenef(){

        require_once('inc/bdd.php');

        $this -> data = array();

        if($req = $db -> query("SELECT * FROM benef_assure WHERE nir_assure = '" . trim($this -> user) . "'")){

            /*
             * Récupère un tableau d'objets
             */
            $compteur = 0;

            while($obj = $req -> fetch_object()){

                $date = new DateTime($obj -> dateNaissance_benef);

                $date_naissance = date_timestamp_get($date) * 1000;

                $array = array(
                    "id" => $compteur ,
                    "nom" => utf8_encode($obj -> nom_benef) ,
                    "prenom" => utf8_encode($obj -> prenom_benef) ,
                    "avatar" => utf8_encode("img/avatars/" . $obj -> avatar_benef) ,
                    "qualite" => utf8_encode($obj -> qualite_benef) ,
                    "sexe" => utf8_encode($obj -> sexe_benef) ,
                    "dateNaissance" => utf8_encode($date_naissance) ,
                    "rang" => utf8_encode($obj -> rang_benef)
                );

                array_push($this -> data , $array);

                $compteur ++;
            }
        }
        $req -> close();
    }

    function __destruct(){

        header('Content-Type: application/json');

        echo json_encode($this -> data);
    }

}
