<?php

/*
  Ce script permet :
  1 - récupérer le détail PS

 */

class Ps{
    /*
      @request : paramètres de l'URL
     */

    public $request;

    /*
      @action : action demandée
     */
    private $action;

    /*
      @ps : num_ps
     */
    private $ps;
    /*
      @nom : nom du ps
     */
    private $nom;

    /*
      @localisation : localisation ps
     */
    private $localisation;

    /*
      $type_spe : code spécialite ps
     */
    private $type_spe;

    /*
      @id : ID du PS
     */
    private $id;

    /*
      @data : données au format JSON
     */
    private $data;

    function __construct($request){

        $this -> request = $request;

        $this -> action = $this -> request['action'];

        switch($this -> action){

            case 'detail_un_ps':

                $this -> ps = $this -> request['numero'];

                $this -> detailPs();

                break;

            case 'search':

//                $this -> type_spe = $this -> request['type'];
//
//                $this -> localisation = $this -> request['localisation'];
//
//                $this -> nom = $this -> request['nom'];

                $this -> searchPs();

                break;

            case 'getProfessions':

                $this -> liste_professions();

                break;

            default:

                break;
        }
    }

    function detailPs(){

        require_once('inc/mock_ps.php');

        $params = array("NumAm" => $this -> ps);

        $return = $client -> __soapCall("callService_DetailPS" , array($params));

        $ps_nom = utf8_encode(trim($return -> ExeNom));
        $ps_prenom = utf8_encode(trim($return -> PfsPrn));
        $ps_telephone = utf8_encode(trim($return -> TelNum));
        $ps_adresse = utf8_encode(trim($return -> VoiLib . ", " . $return -> BdiCod . " " . $return -> RsdLib));
        $this -> data = array("numero" => $this -> ps , "titre" => $ps_nom . " " . $ps_prenom , "telephone" => $ps_telephone , "adresse" => $ps_adresse);
    }

    function searchPs(){

        require_once('./inc/bdd.php');

        $conditions = "";

        $this -> data = array();

//        if($this -> type_spe != "undefined")
//            $conditions.=" and ps_code_spe='" . $this -> type_spe . "' ";
//
//        if($this -> nom != "undefined")
//            $conditions.=" and ps_nom like '%" . $this -> nom . "%' ";
//
//        if($this -> localisation != "undefined")
//            $conditions.=" and ps_adresse like '%" . $this -> localisation . "%' ";

        if($req = $db -> query("SELECT * FROM ps where 1=1 " . $conditions)){

            /*
             * Récupère un tableau d'objets
             */
            while($obj = $req -> fetch_object()){

                $array = array("numero" => $obj -> ps_num ,
                    "nom" => utf8_encode($obj -> ps_nom) ,
                    "prenom" => utf8_encode($obj -> ps_prenom) ,
                    "telephone" => utf8_encode($obj -> ps_telephone) ,
                    "honoraire" => utf8_encode($obj -> type_conv) ,
                    "cv" => utf8_encode($obj -> cv) ,
                    "adresse" => utf8_encode($obj -> ps_adresse));

                array_push($this -> data , $array);
            }
        }
    }

    function liste_professions(){

        require_once('./inc/bdd.php');

        $this -> data = array();

        if($req = $db -> query("SELECT * FROM specialite group by libelle_spe order by libelle_spe asc")){

            /* Récupère un tableau d'objets */

            while($obj = $req -> fetch_object()){

                $array = array("id" => $obj -> code_spe ,
                    "group" => utf8_encode($obj -> group) ,
                    "libelle" => utf8_encode($obj -> libelle_spe));

                array_push($this -> data , $array);
            }
        }
    }

    function __destruct(){

        header('Content-Type: application/json');

        echo json_encode($this -> data);
    }

}
