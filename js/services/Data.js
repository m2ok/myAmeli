/*
 * Data : ce service gèrent les requêtes REST
 */
Services.factory('Data', ['$resource', '$filter', '$routeParams', '$log', 'localStorageService', 'Notification',
    function ($resource, $filter, $routeParams, $log, localStorageService, Notification){

        /*
         * @urlDataBeneficiaires : URL des données bénéficiaires
         */
        //var urlDataBeneficiaires = './rest/index.php';
        var urlDataBeneficiaires = './datas/data-beneficiaires.json';

        /*
         * @urlDataPaiement  : URL des données de remboursement
         */
        //var urlDataPaiement = './rest/index.php';
        var urlDataPaiement = './datas/data-paiement.json';

        /*
         * @urlDataPsProfessions : URL des données sur les catégories de PS
         */
        //var urlDataPsProfessions = '.rest/index.php';
        var urlDataPsProfessions = './datas/data-profession.json';

        /*
         * @urlDataPs : URL des données sur les PS
         */
        //var urlDataPs = './rest/index.php';
        var urlDataPs = './datas/data-ps.json';

        /*
         * @urlDataPsDetail : URL des données sur un PS
         */
        //var urlDataPsDetail = './rest/index.php';
        var urlDataPsDetail = './datas/data-ps-detail.json';

        /*
         * @urlDataRdvDemo : URL des données de RDV pour la démo
         */
        var urlDataDemo = './datas/data-rdv.json';

        /*
         * @defaultDateRappelPostRdv : délai après lequel on demande à l'utilisateur des informations
         * complémentaires sur son RDV
         * + 1 heure
         */
        var defaultDateRappelPostRdv = (1000 * 60 * 60);

        /*
         * Super class
         */
        var request = {
            /*
             * Vérifie si les données des RDV sont dans localStorage
             */
            isDataRdv : function (){

                var status = false;

                if(localStorageService.isSupported){

                    /*
                     * LocalStorage est supporté
                     */
                    if(localStorageService.get("data-rdv") !== null){

                        /*
                         * Les données sont présentes dans localStorage
                         */
                        status = true;
                    }
                }

                return status;
            },
            /*
             * Liste tous les RDV de l'assuré et de ses bénéficiaires
             */
            getRdvList : function (){

                /*
                 * Les données ne sont pas dans localStorage
                 */
                var data = request.isDataRdv() === true ? localStorageService.get("data-rdv") : [];

                /*
                 * @lastData : date des dernières données sauvegardées
                 */
                var dateData = localStorageService.isSupported && localStorageService.get("data-rdv-date") !== null ? localStorageService.get("data-rdv-date") : new Date().getTime();

                /*
                 * @arrData : objet qui contient les données mises à jour
                 */
                var arrData = [];

                /*
                 * On parcoure les données à la recherche de nouveau paiement
                 */
                angular.forEach(data, function (item){

                    /*
                     * VERIFICATION SI PAIEMENT DE L'ASSURANCE MALADIE
                     * 1 - La date du RDV est passée
                     * 2 - Les informations post RDV ont été saisies
                     * 3 - Le paiement n'a pas encore eu lieu
                     */
                    if(item.date <= new Date().getTime()
                        && item.etat === false
                        && (item.montantPaye === null || item.typeFeuille === null)){

                        $log.info("Vérification si paiement");

                        var paiement = request.getPaiement({
                            date : item.date,
                            numero : item.ps.numero,
                            nom : item.beneficiaire.nom,
                            prenom : item.beneficiaire.prenom,
                            dateNaissance : item.beneficiaire.dateNaissance,
                            rang : item.beneficiaire.rang,
                            montantPaye : item.montantPaye,
                            typeFeuille : item.typeFeuille,
                            dateEnvoiFSP : item.dateEnvoiFSP
                        });

                        /*
                         * S'il y'a eu un paiement, on met à jour les informations sur le
                         * remboursement
                         */
                        if(paiement.length > 0){

                            $log.info("Un paiement a eu lieu");

                            item.etat = true;

                            item.remboursement.montant = paiement.montant,
                                item.remboursement.taux = paiement.taux,
                                item.remboursement.montantAS = paiement.montantAS,
                                item.remboursement.montantMutuelle = paiement.montantMutuelle,
                                item.remboursement.montantPF = paiement.montantPF,
                                item.remboumontant.montantFR = paiement.montantFR,
                                item.remboursement.date = paiement.date;
                        }
                    }

                    /*
                     * NOTIFICATION DE RAPPEL
                     *
                     * Envoi une notification si :
                     * 1 - les notifications sont activées
                     * 2 - la date du jour est égale ou supérieur à la date de rappel
                     * 3 - le rappel n'a pas encore été notifié
                     */
                    if(Notification.isNotification() === true
                        && new Date().getTime() >= item.dateRappel
                        && item.dateRappel !== null
                        && item.rappel === false){

                        /*
                         * Envoi une notification via le service Notification
                         */
                        Notification.send({
                            id : item.id,
                            titre : "Rappel de rendez-vous",
                            body : "Un rendez-vous arrive à échéance. Cliquez pour en savoir plus.",
                            icon : item.beneficiaire.avatar
                        });

                        /*
                         * Une fois la notification envoyée, on change le status de celle-ci
                         */
                        item.rappel = true;
                    }

                    /*
                     * NOTIFICATION POST RDV
                     *
                     * Envoi une notification si :
                     * 1 - les notifications sont activées
                     * 2 - la date du RDV est passée d'au moins + d'1 heure
                     * 3 - la date du RDV inférieure ou égale à la date du jour
                     * 4 - Le montant payé n'est pas renseigné
                     * 5 - Le type de feuille de soins n'est pas renseigné
                     * 6 - Si FSP, la date d'envoi de la feuille doit être renseigné
                     * 7 - Le remboursement n'a pas encore été effectué
                     */
                    if(Notification.isNotification() === true
                        && item.date <= (item.date + defaultDateRappelPostRdv)
                        && item.date <= new Date().getTime()
                        && item.etat === false
                        && (item.montantPaye === null || item.typeFeuille === null)
                        && item.rappelPostRdv === false){

                        /*
                         * Envoi une notification via le service Notification
                         */
                        Notification.send({
                            id : item.id,
                            titre : "Informations complémentaires",
                            body : "Renseignez les informations complémentaires sur votre RDV.",
                            icon : item.beneficiaire.avatar
                        });

                        /*
                         * Une fois la notification envoyée on change le status de celle-ci
                         */
                        item.rappelPostRdv = true;
                    }

                    /*
                     * NOTIFICATION DE REMBOURSEMENT
                     *
                     * Envoi une notification si :
                     * 1 - les notifications sont activées
                     * 2 - la date des dernières données est connue
                     * 4 - le remboursement SS a été effectué
                     * 5 - la notification n'a pas encore été envoyée
                     */
                    if(Notification.isNotification() === true
                        && dateData !== null
                        && item.remboursement.date !== null
                        && item.etat === true
                        && item.notification === false){

                        /*
                         * Envoi une notification via le service Notification
                         */
                        Notification.send({
                            id : item.id,
                            titre : "Nouveau remboursement",
                            body : "Vous avez reçu un remboursement d'un montant de " + item.remboursement.montant + "€.",
                            icon : item.beneficiaire.avatar
                        });

                        /*
                         * Une fois la notification envoyée on change le status de celle-ci
                         */
                        item.notification = true;
                    }

                    /*
                     * Insère l'élement dans l'objet
                     */
                    arrData.push(item);
                });

                /*
                 * On sauvegarde les données dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("data-rdv", arrData);

                    localStorageService.set("data-rdv-date", dateData);
                }

                /*
                 * Renvoi l'objet
                 */
                return arrData;
            },
            /*
             * Renvoie le max ID des données RDV
             */
            getLastRdvId : function (){

                /*
                 * Récupère les données RDV
                 */
                var data = request.getRdvList();

                /*
                 * S'il n'y pas encore de données
                 * renvoie l'ID 0
                 */
                if(data.length === 0){

                    return -1;
                }

                /*
                 * Crée un tableau contenant tous les ID
                 */
                var arrId = Object.keys(data).map(function (k){

                    return data[k].id;
                });

                /*
                 * Tri décroissant des ID
                 */
                arrId.sort(function (a, b){

                    return b - a;
                });

                /*
                 * ID le plus élevé
                 */
                return arrId[0];
            },
            /*
             * Récupère le détail d'un RDV
             */
            getRdvDetail : function (params){

                /*
                 * @data : tous les RDV
                 */
                var data = request.getRdvList();

                /*
                 * @result : détail d'un RDV
                 */
                var result = $filter("filter")(data, {id : parseInt($routeParams.id, 0)})[0];

                return result;
            },
            /*
             * Supprimer un RDV
             */
            deleteRdv : function (){

                /*
                 * Récupère les données RDV
                 */
                var data = request.getRdvList();

                /*
                 * Nouvel objet qui contient les RDV
                 */
                var arrData = [];

                /*
                 * Supprime le RDV par son ID dans la liste des RDV
                 */
                arrData = data.filter(function (item){

                    return item.id !== parseInt($routeParams.id, 0);
                });

                /*
                 * Mise à jour des donnés dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("data-rdv", arrData);
                }
            },
            /*
             * Mise à jour d'un RDV
             * @params : objet qui contient les élements à mettre à jour
             */
            updateRdv : function (params){

                var data = request.getRdvList();

                /*
                 * Nouvel objet qui contient les RDV
                 */
                var arrData = [];

                angular.forEach(data, function (item){

                    if(item.id === parseInt($routeParams.id)){

                        angular.forEach(params, function (value, param){

                            /*
                             * Met à jour l'élément du RDV avec la nouvelle valeur
                             */
                            item[param] = value;
                        });
                    }

                    arrData.push(item);
                });

                /*
                 * Mise à jour des donnés dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("data-rdv", arrData);
                }
            },
            /*
             * Crée un nouveau RDV
             */
            createRdv : function (obj){

                /*
                 * Récupère la liste des RDV
                 */
                var data = request.getRdvList();

                /*
                 * Ajoute le nouveau RDV
                 */
                data.push(obj);

                /*
                 * Enregistrement et mise à jour dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("data-rdv", data);

                    localStorageService.set("data-rdv-date", new Date().getTime());
                }

                return true;
            },
            /*
             * Vérifie si les données sur les bénéficiaires sont dans localStorage
             */
            isDataBeneficiaires : function (){

                var status = false;

                if(localStorageService.isSupported){

                    /*
                     * LocalStorage est supporté
                     */
                    if(localStorageService.get("data-beneficiaires") !== null){

                        /*
                         * Les données sont présentes dans localStorage
                         */
                        status = true;
                    }
                }

                return status;
            },
            /*
             * Liste tous les bénéficiaires du compte
             */
            getBeneficiairesList : function (){

                /*
                 * Données chargées en local
                 */
                if(request.isDataBeneficiaires() === true){

                    return localStorageService.get("data-beneficiaires");
                }

                /*
                 * Données chargées en distant
                 */
                var resource = $resource(urlDataBeneficiaires, {}, {
                    query : {
                        method : 'GET',
                        params : {
                            user : request.getUser(), // Numéro de sécurité sociale
                            route : "assure",
                            action : "getBenefs"
                        },
                        isArray : true,
                        responseType : 'json',
                        transformResponse : function (data){

                            /*
                             * Sauvegarde les données dans localStorage
                             */
                            if(localStorageService.isSupported){

                                localStorageService.set("data-beneficiaires", data);
                            }

                            return data;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            },
            /*
             * Met à jout les informations d'un bénéficiaires
             * @params : objet qui contient l'élement à mettre à jour
             */
            updateBeneficiaire : function (params){

                /*
                 * @data : données existantes
                 */
                var data = request.getBeneficiairesList();

                /*
                 * Mise à jour de la combinaison element/valeur
                 */
                data[params.id][params.element] = params.valeur;

                /*
                 * Sauvegarde des données modifiées
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("data-beneficiaires", data);
                }

                /*
                 * Si l'élément modifié est l'avatar, on propage la modification
                 * dans les données RDV
                 */
                if(params.element === 'avatar'){

                    $log.log(params);

                    /*
                     * @data : données des RDV existantes
                     */
                    var data = request.getRdvList();

                    /*
                     * @arrData : nouvelles données
                     */
                    var arrData = [];

                    angular.forEach(data, function (item){

                        if(item.beneficiaire.id === params.id){

                            /*
                             * Met à jour l'avatar du bénéficiaire
                             */
                            item.beneficiaire.avatar = params.valeur;
                        }

                        arrData.push(item);
                    });

                    /*
                     *  Si localStorage est supporté on l'utilise
                     */
                    if(localStorageService.isSupported){

                        /*
                         * On sauvegarde les datas dans localStorage pour optimiser
                         */
                        localStorageService.set("data-rdv", arrData);

                        /*
                         * On sauvegarde la date de la mise à jour des données
                         */
                        localStorageService.set("data-rdv-date", new Date().getTime());
                    }
                }
            },
            /*
             * Récupère les informations sur le remboursement
             */
            getPaiement : function (params){

                /*
                 * Données chargées en distant
                 */
                var resource = $resource(urlDataPaiement, {}, {
                    query : {
                        method : 'GET',
                        params : {
                            user : request.getUser(), // numéro de sécurité sociale
                            date : params.date, // date du rendez-vous
                            numero : params.numero, // numéro du PS
                            nom : params.nom, // nom du bénéficiaire
                            prenom : params.prenom, // prénom du bénéficiaire
                            dateNaissance : params.dateNaissance, // date de naissance du bénéficiaire
                            rang : params.rang, // rang du bénéficiaire
                            montantPaye : params.montantPaye, // montant payé au PS
                            typeFeuille : params.typeFeuille, // type de feuille de soins
                            dateEnvoiFSP : params.dateEnvoiFSP // date d'envoi de la FSP
                        },
                        isArray : true,
                        responseType : 'json',
                        transformResponse : function (data){

                            /*
                             * Si aucune donnée retourne un objet vide
                             */
                            return data;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            },
            /*
             * Vérifie le login et mot de passe
             *
             */
            loginUser : function (params){

                if(localStorageService.isSupported){

                    localStorageService.set("user", params.nir);
                }

                /*
                 * Chargement des données Bénéficiaires
                 */
                request.getBeneficiairesList();

                /*
                 * Chargement des données RDV
                 */
                request.getRdvList();

                return true;
            },
            /*
             * Retourne le NIR de l'utilisateur courant depuis localStorage
             */
            getUser : function (){

                if(localStorageService.isSupported){

                    if(localStorageService.get("user") !== null){

                        return localStorageService.get("user");
                    }
                }

                return false;
            },
            /*
             * Est-ce la première visite ?
             */
            isFirstVisit : function (){

                if(localStorageService.isSupported){

                    if(localStorageService.get("is-first-visit") !== null){

                        return false;
                    }else{

                        localStorageService.set("is-first-visit", false);

                        return true;
                    }
                }

                return true;
            },
            /*
             * Réinitialise l'application
             * Vide toutes les données de localStorage
             */
            clearCache : function (){

                if(localStorageService.isSupported){

                    localStorageService.clearAll();
                }

                return true;
            },
            /*
             * Retourne la liste des catégories des PS
             */
            getPsProfessions : function (){
                /*
                 * Données chargées en distant
                 */
                var resource = $resource(urlDataPsProfessions, {}, {
                    query : {
                        method : 'GET',
                        params : {
                            route : 'ps',
                            action : 'getProfessions'
                        },
                        isArray : true,
                        responseType : 'json',
                        transformResponse : function (data){

                            return data;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            },
            /*
             * Retourne la liste des PS en fonction des critères de recherche
             */
            getPsList : function (params){

                /*
                 * Données chargées en distant
                 */
                var resource = $resource(urlDataPs, {}, {
                    query : {
                        method : 'GET',
                        params : {
                            route : 'ps',
                            action : 'search',
                            type : params.type,
                            profession : params.profession,
                            localisation : params.localisation,
                            acte : params.acte,
                            cv : params.cv,
                            sexe : params.sexe,
                            honoraires : params.honoraires
                        },
                        isArray : true,
                        responseType : 'json',
                        transformResponse : function (data){

                            /*
                             * On crée ce tableau pour y ajouter des données manipulées
                             */
                            var arrItems = [];

                            angular.forEach(data, function (item){

                                /*
                                 * On regarde si le PS ou l'établissement fait partie des favoris
                                 */
                                $favori = "img/star-grey.svg";

                                $isFavori = false;

                                if(request.isFavori(item.numero)){

                                    $favori = "img/star-yellow.svg";

                                    $isFavori = true;
                                }

                                /*
                                 *  @nom :
                                 *   - Nom du PS
                                 *   - Raison sociale de l'établissement
                                 *
                                 *  @prenom :
                                 *   - Prénom du PS
                                 *   - Vide si établissement
                                 *
                                 *  @telephone :
                                 *   - Téléphone du PS ou de l'établissement
                                 *
                                 *  @adresse :
                                 *   - Adresse du PS ou de l'établissement
                                 *
                                 *  @honoraire :
                                 *   - type d'honoraire du PS
                                 *
                                 *  @cv :
                                 *   - true : si le PS prend la carte vitale
                                 *   - false : si le PS ne prend pas la carte vitale
                                 *
                                 */
                                arrItems.push({
                                    "numero" : item.numero,
                                    "nom" : item.nom,
                                    "prenom" : item.prenom,
                                    "telephone" : item.telephone,
                                    "adresse" : item.adresse,
                                    "honoraire" : item.honoraire,
                                    "cv" : item.cv,
                                    "favori" : $favori,
                                    "isFavori" : $isFavori
                                });
                            });

                            return arrItems;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            },
            /*
             * Retourne les coordonnées d'un PS ou un établissement
             */
            getPsDetail : function (){

                /*
                 * Données chargées depuis le JSON pour la démo
                 */
                var resource = $resource(urlDataPs, {}, {
                    query : {
                        method : 'GET',
                        params : {},
                        isArray : false,
                        responseType : 'json',
                        transformResponse : function (data){

                            /*
                             * Filtre sur le numéro du PS qui nous intéresse
                             */
                            var result = $filter("filter")(data, {numero : $routeParams.numero})[0];

                            /*
                             * Concaténation du Nom et du Prénom
                             */
                            result.titre = 'Dr. ' + result.prenom + ' ' + result.nom;

                            return result;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();

                /*
                 * @result : détail sur un PS
                 */
                var result = $filter("filter")(data, {numero : parseInt($routeParams.numero, 0)})[0];

                return result;

                /*
                 * Données chargées en distant
                 */
                var resource = $resource(urlDataPsDetail, {}, {
                    query : {
                        method : 'GET',
                        params : {
                            numero : $routeParams.numero, // Numéro deu PS récupéré depuis l'URL
                            route : 'ps',
                            action : 'detail_un_ps'
                        },
                        isArray : false,
                        responseType : 'json',
                        transformResponse : function (data){

                            return data;
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            },
            /*
             * Retourne la liste des favoris
             */
            getFavorisList : function (){

                if(localStorageService.isSupported){

                    if(localStorageService.get("favoris") !== null){

                        return localStorageService.get("favoris");
                    }else{

                        return [];
                    }
                }

                return false;
            },
            /*
             * Ajoute le PS ou l'établissement dans les favoris
             */
            setFavori : function (params){
                /*
                 * Récupération des favoris stockés dans LocalStorage
                 * et ajout du nouvel élément
                 */
                var arrData = request.getFavorisList();

                arrData.push(params);

                /*
                 * Mise à jour des donnés dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("favoris", arrData);
                }
            },
            /*
             * Ajoute le PS ou l'établissement dans les favoris
             */
            deleteFavori : function (param){
                /*
                 * On récupère les favoris stockés dans LocalStorage
                 */
                var data = request.getFavorisList();

                /*
                 * On parcourt la liste des favoris pour constituer une nouvelle liste
                 * qui ne contiendra pas l'élément passé en paramètre
                 */
                var arrData = [];

                angular.forEach(data, function (item){

                    if(item.numero !== param){

                        arrData.push(item);
                    }
                });

                /*
                 * Mise à jour des donnés dans localStorage
                 */
                if(localStorageService.isSupported){

                    localStorageService.set("favoris", arrData);
                }
            },
            /*
             * Retourne true si le PS ou l'établissement est dans les favoris
             */
            isFavori : function (param){
                /*
                 * On récupère les favoris stockés dans LocalStorage
                 */
                var data = request.getFavorisList();

                var retour = false;

                /*
                 * On parcourt la liste des favoris pour chercher le PS ou l'établissement passé en paramètre
                 */
                angular.forEach(data, function (item){

                    if(item.numero === param){
                        retour = true;
                    }
                });

                return retour;
            },
            /*
             * Permet de charger un jeu de données pour la démo
             */
            loadDemoMode : function (){

                var resource = $resource(urlDataDemo, {}, {
                    query : {
                        method : 'GET',
                        params : {},
                        isArray : true,
                        responseType : 'json',
                        transformResponse : function (data){

                            if(localStorageService.isSupported){

                                localStorageService.set('data-rdv', data);

                                $log.log('Les données de démo sont chargées et stockées dans localStorage');
                            }
                        }
                    }
                });

                /*
                 * Renvoi les données
                 */
                return resource.query();
            }
        };

        return request;
    }]);