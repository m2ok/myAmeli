/*
 * Notification : ce service gèrent les notifications
 */
Services.factory('Notification', ['$rootScope', '$timeout', '$filter', 'localStorageService', '$window', '$log',
    function ($rootScope, $timeout, $filter, localStorageService, $window, $log){

        var notification = {
            /*
             * Vérifie si la fonctionnalité Notification est supportée par le navigateur et l'OS
             * et si l'utilisateur a activé la fonctionnalité dans les paramètres de l'application
             */
            isNotification : function (){

                var status = false;

                if(typeof window.Notification !== 'undefined'){

                    status = true;

                    if(localStorageService.isSupported){

                        /*
                         * Vérifie si les notifications sont activées
                         */
                        if(localStorageService.get('notifications') === "false"){

                            status = false;
                        }
                    }
                }

                return status;
            },
            /*
             * Attention : ne semble pas fonctionner sur tous les smartphones
             * Si c'est un mobile on envoie une notification à la manière de l'OS
             * Android
             */
            isMobile : function (){

                var status = false;

                var mobile = navigator.userAgent.toLowerCase().match(/android|iphone|ipad|ipod/i);

                if(mobile){

                    $log.warn("mobile : " + mobile);

                    status = true;
                }

                return status;
            },
            /*
             * Permet d'activer/désactiver les notifications
             */
            switch : function (){

                var status = false;

                if(localStorageService.isSupported){

                    if(localStorageService.get("notifications") === null || localStorageService.get("notifications") === "true"){

                        localStorageService.set("notifications", false);

                        status = false;

                    }else{

                        localStorageService.set("notifications", true);

                        status = true;
                    }
                }

                return status;
            },
            /*
             * Envoi une notification
             *
             * @params  = {
             *      titre : titre de la notification
             *      body : message
             *      icon : url vers une image
             *	}
             */
            send : function (params){

                /*
                 * Notification desktop
                 */
                if(notification.isNotification() === true && notification.isMobile() === false){

                    /*
                     * @instance : nouvelle notification
                     */
                    var instance = null;

                    if(Notification.permission === "granted"){

                        /*
                         * Charge et envoie la notification
                         */
                        instance = new Notification(
                            params.titre, {
                                body : params.body,
                                icon : params.icon
                            }
                        );

                    }else{

                        /*
                         * Demande l'autorisation à l'utilisateur
                         */
                        Notification.requestPermission(function (permission){

                            if(permission === "granted"){

                                /*
                                 * Charge et envoie la notification
                                 */
                                instance = new Notification(
                                    params.titre, {
                                        body : params.body,
                                        icon : params.icon
                                    }
                                );
                            }
                        });
                    }

                    /*
                     * Actions sur la notification
                     */
                    instance.onclick = function (id){

                        /*
                         * Redirection vers la page du détail du RDV
                         */
                        if(params.test !== true){

                            $window.location.hash = '#/rdv-detail/' + params.id;
                        }
                    };

                    instance.onshow = function (){

                        /*
                         * Masquer les notifications après un délai de 5 secondes
                         */
                        setTimeout(instance.close.bind(instance), 5000);
                    };

                    instance.onclose = function (id){
                        /*
                         * Ne rien faire
                         */
                    };

                    instance.onerror = function (){

                        $log.warn('Erreur sur la notification');
                    };
                }

                /*
                 * Notification mobile
                 */
                if(notification.isNotification() === true && notification.isMobile() === true){

                    /*
                     * Modèle de notification
                     */
                    $rootScope.notificationUrl = 'views/notifications/notification.html';

                    /*
                     * Personnalisation de la notification
                     */
                    $rootScope.notification = {
                        titre : params.titre,
                        body : params.body,
                        icon : params.icon
                    };

                    /*
                     * Disparition de la notification après un délai
                     */
                    $timeout(function (){

                        $rootScope.notificationUrl = null;
                    }, 5000);

                    /*
                     * Quand l'utilisateur clique sur la notification
                     */
                    $rootScope.onNotificationClick = function (id){

                        if(params.test === true){

                            $rootScope.notificationUrl = null;
                        }else{

                            $window.location.hash = '#/rdv-detail/' + params.id;
                        }
                    };


                }
            }
        };

        return notification;
    }]);