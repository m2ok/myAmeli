/*
 * Tutoriel : Présentation des fonctionnalités
 */
Controllers.controller('TutorielCtrl', ['$scope', '$window', '$log', 'Data',
    function ($scope, $window, $log, Data){

        /*
         * On charge le jeu de données RDV pour la démo
         */
        Data.loadDemoMode();

        $scope.items = [
            {
                id : 1,
                titre : "myAmeli",
                message : "Gérez vos rendez-vous médicaux et soyez informé des délais de remboursement.",
                bouton : {
                    titre : 'Suivant',
                    href : '#/tutoriel'
                }
            },
            {
                id : 2,
                titre : "Trouvez un professionnel de santé ou un établissement près de chez vous",
                message : "Recherchez, contactez et prenez rendez-vous  auprès des professionnels de santé et des établissements de soins de la France entière.",
                bouton : {
                    titre : 'Suivant',
                    href : '#/tutoriel'
                }
            }, {
                id : 3,
                titre : "Notifications de paiements",
                message : "Soyez notifié de l'arrivée d'un remboursement en temps réel et ne perdez plus de temps à vérifier vos décomptes.",
                bouton : {
                    titre : 'Suivant',
                    href : '#/tutoriel'
                }
            }, {
                id : 4,
                titre : "Lancez-vous !",
                message : "Créez votre premier rendez-vous et découvrez des fonctionnalités qui vous feront gagnez du temps.",
                bouton : {
                    titre : 'Allons-y !',
                    href : '#/rdv-list'
                }
            }
        ];

        /*
         * @etape
         */
        $scope.etape = 1;

        /*
         * suivant() : passe au panneau du tutoriel suivant
         * Une fois le tutoriel fini on redirige vers ps-search
         */
        $scope.suivant = function (){

            $scope.etape++;
        };

    }]);