/*
 * About : A propos
 */
Controllers.controller('AboutCtrl', ['$scope', '$document', '$window', '$log',
    function ($scope, $document, $window, $log){

        /*
         * Tableau
         */
        $scope.arr = [];

        $document.bind('keypress', function (event){

            /*
             * Ajout
             */
            $scope.arr.push(event.keyCode);

            /*
             * Si le tableau contient 3 éléments
             */
            if($scope.arr.length === 3){

                /*
                 * Vérification
                 */
                if($scope.arr.indexOf(105) !== -1
                    && $scope.arr.indexOf(99) !== -1
                    && $scope.arr.indexOf(112) !== -1){

                    $scope.icp = true;
                }
                /*
                 * Vidage
                 */
                $scope.arr = [];
            }
        });

        /*
         * Efface
         */
        $scope.clear = function(){

            $scope.icp = false;
        };

    }]);