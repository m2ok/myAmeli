/*
 * SearchCtrl : permet de rechercher, filtrer et choisir un PS ou un établissement
 */
Controllers.controller('PsSearchCtrl', ['$scope', '$document', '$window', '$timeout', '$log', 'Data',
    function ($scope, $document, $window, $timeout, $log, Data){

        /*
         * Récupère la liste des professions depuis le service Data
         */
        $scope.optionsProfession = Data.getPsProfessions();

        /*
         * Pour palier au délai de chargement des données dans le scope
         */
        $timeout(function (){

            /*
             * Sélection de Médecin généraliste par défaut
             */
            $scope.profession = $scope.optionsProfession[41].id;
        }, 500);

        /*
         * switchMoreOptions() : affiche/cache les options de recherche supplémentaires
         */
        $scope.searchMoreOptions = false;

        $scope.moreOptionsText = "Afficher plus d'options de recherche";

        $scope.switchMoreOptions = function (){

            $scope.searchMoreOptions = $scope.searchMoreOptions === true ? false : true;

            $scope.moreOptionsText = $scope.searchMoreOptions === true ? "Afficher moins d'options de recherche" : "Afficher plus d'options de recherche";
        };

        /*
         * Chargement des options et selection de l'option par défaut
         */
        $scope.nom = null;

        $scope.acte = null;

        $scope.localisation = null;

        $scope.optionsType = ["Professionnel de santé", "Etablissements"];

        $scope.type = $scope.optionsType[0];

        $scope.optionsCv = ["Carte vitale - Indifférent", "Oui", "Non"];

        $scope.cv = $scope.optionsCv[0];

        $scope.optionsHonoraires = [
            "Type d'honoraires - Indifférent",
            "Honoraires sans dépassements",
            "Honoraires avec dépassements maîtrisés",
            "Honoraires libres",
            "Non conventionné"];

        $scope.honoraires = $scope.optionsHonoraires[0];

        $scope.optionsSexe = ["Sexe - Indifférent", "Homme", "Femme"];

        $scope.sexe = $scope.optionsSexe[0];

        /*
         * searchPs() : Fonction de recherche d'un PS ou d'un établissement
         */
        $scope.searchLaunched = false;

        $scope.searchPs = function (){

            /*
             * Indique qu'une recherche vient d'être effectuée
             */
            $scope.searchLaunched = true;

            /*
             * @items : Contient tous les résultats de la recherche récupérés via le service Data
             */
            $scope.items = Data.getPsList({
                type : $scope.type,
                profession : $scope.profession,
                localisation : $scope.localisation,
                acte : $scope.acte,
                cv : $scope.cv,
                sexe : $scope.sexe,
                honoraires : $scope.honoraires
            });

            /*
             * Scroll de la page vers le haut pour laisser apparaître les résultats
             */
            $timeout(function (){

                $window.scrollTo(0, 480);
            }, 100);
        };

        /*
         * ajouterFavori() : ajoute le PS ou l'établissement dans les favoris.
         */
        $scope.ajouterFavori = function ($index){

            if($scope.items[$index].favori === "img/star-grey.svg"){

                /*
                 * Met à jour l'icône
                 */
                $scope.items[$index].favori = "img/star-yellow.svg";

                /*
                 * Ajoute le favori dans localStorage
                 */
                Data.setFavori({
                    numero : $scope.items[$index].numero,
                    nom : $scope.items[$index].nom,
                    prenom : $scope.items[$index].prenom,
                    adresse : $scope.items[$index].adresse,
                    telephone : $scope.items[$index].telephone,
                    honoraire : $scope.items[$index].honoraire,
                    cv : $scope.items[$index].cv,
                    favori : $scope.items[$index].favori
                });
            }else{

                $scope.items[$index].favori = "img/star-grey.svg";

                /*
                 * Supprime le favori de localStorage
                 */
                Data.deleteFavori($scope.items[$index].numero);
            }
        };

        /*
         * planifierRdv() : redirige vers le formulaire de création d'un RDV.
         */
        $scope.planifierRdv = function ($index){

            /*
             * Redirige vers la vue rdv-create
             */
            $window.location.hash = '#/rdv-create/' + $scope.items[$index].numero;
        };

    }]);
